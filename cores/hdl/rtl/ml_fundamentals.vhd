library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
-- ML_FUNDAMENTALS
-- package for basics like d flip-flops, overloaded muxes, single-pulsers, and
-- stickybits
--
-- contains
--      dff...
-------------------------------------------------------------------------------

package ml_fundamental is

  procedure dff (
    signal d          : in  std_logic;
    signal q          : out std_logic;
    constant INIT_VAL : in  std_logic;
    signal EN         : in  std_logic;
    signal clk        : in  std_logic;
    signal rst        : in  std_logic);

end package;

package body ml_fundamental is

  procedure dff (
    signal d          : in  std_logic;
    signal q          : out std_logic;
    constant INIT_VAL : in  std_logic;
    signal EN         : in  std_logic;
    signal clk        : in  std_logic;
    signal rst        : in  std_logic) is

  begin
    if(rising_edge(clk)) then
      if(rst /= '0') then
        q <= INIT_VAL;
      else
        q <= d;
      end if;
    end if;
  end dff;

end ml_fundamental;
